<?php

/**
 * Fired during plugin activation
 *
 * @link       yoursite.lv
 * @since      1.0.0
 *
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/includes
 * @author     MF <yoursite.lv>
 */
class Pretty_Tables_Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		require_once PRETTY_TABLES_BASE_DIR . 'includes/class-pretty-tables-post-types.php';
		//Register CPT
		$plugin_post_type = new Pretty_Tables_Post_Types(PRETTY_TABLES_NAME, PRETTY_TABLES_VERSION);
		$plugin_post_type->init();


		//Flush permalinks
		flush_rewrite_rules();
	}
}
