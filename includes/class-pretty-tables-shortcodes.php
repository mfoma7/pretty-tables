<?php
// If this file is called directly, abort.
if (!defined('ABSPATH')) {
    die;
}
/**
 * Plugin Shortcode Functionality
 *
 */
if (!class_exists('Pretty_Tables_Shortcodes')) :
    class Pretty_Tables_Shortcodes
    {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * @var it will be all the css for all shortcodes
         */
        protected $shortcode_css;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         *
         * @param      string $plugin_name The name of the plugin.
         * @param      string $version The version of this plugin.
         */
        public function __construct($plugin_name, $version)
        {

            $this->plugin_name = $plugin_name;
            $this->version     = $version;
            $this->setup_hooks();
        }

        public function setup_hooks()
        {
            add_action('wp_enqueue_scripts', array($this, 'register_style'));

            // add_action('get_footer', array($this, 'enqueue_scripts'));
        }

        public function register_style()
        {
            wp_enqueue_style($this->plugin_name . '-shortcodes', PRETTY_TABLES_PLUGIN_URL . 'public/css/pretty-tables-shortcodes.css', array(), $this->version, 'all');
        }

        // public function enqueue_scripts()
        // {
        //     wp_enqueue_style($this->plugin_name . '-shortcodes');
        // }

        public function show_table($atts)
        {
            $atts = shortcode_atts(
                array(
                    'limit' => get_option('posts_per_page'),
                ),
                $atts,
                'show_table'
            );

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

            $loop_args = array(
                'post_type' => 'pretty-tables',
                'posts_per_page' => $atts['limit'],
                'paged' => $paged,
            );

            $loop = new WP_Query($loop_args);

            ob_start();
?>

            <div class="show_table">
                <?php
                while ($loop->have_posts()) :

                    $loop->the_post();

                    include PRETTY_TABLES_BASE_DIR . 'templates/show-tables/pretty-tables-shortcode.php';

                endwhile;
                wp_reset_postdata();
                ?>
            </div>
<?php
            return ob_get_clean();
        }
    }
endif;
