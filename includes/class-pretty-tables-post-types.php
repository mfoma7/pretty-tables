<?php

/**
 *
 * @link       yoursite.lv
 * @since      1.0.0
 *
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/public
 */

/**
 * Functionality for our custom post types
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/public
 * @author     MF <yoursite.lv>
 */
class Pretty_Tables_Post_Types
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    public function init()
    {
        $this->register_cpt_pretty_tables();
    }

    public function register_cpt_pretty_tables()
    {
        $labels = array(
            'name'                  => _x('pretty tables [show_table]', 'Post type general name', 'pretty-tables'),
            'singular_name'         => _x('pretty table', 'Post type singular name', 'pretty-tables'),
            'menu_name'             => _x('pretty tables', 'Admin Menu text', 'pretty-tables'),
            'name_admin_bar'        => _x('pretty table', 'Add New on Toolbar', 'pretty-tables'),
            'add_new'               => __('Add New', 'pretty-tables'),
            'add_new_item'          => __('Add New table', 'pretty-tables'),
            'new_item'              => __('New table', 'pretty-tables'),
            'edit_item'             => __('Edit table', 'pretty-tables'),
            'view_item'             => __('View table', 'pretty-tables'),
            'all_items'             => __('All tables', 'pretty-tables'),
            'search_items'          => __('Search tables', 'pretty-tables'),
            'parent_item_colon'     => __('Parent table:', 'pretty-tables'),
            'not_found'             => __('No tables found.', 'pretty-tables'),
            'not_found_in_trash'    => __('No tables found in Trash.', 'pretty-tables'),
            'featured_image'        => _x('table Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'pretty-tables'),
            'set_featured_image'    => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'pretty-tables'),
            'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'pretty-tables'),
            'use_featured_image'    => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'pretty-tables'),
            'archives'              => _x('table archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'pretty-tables'),
            'insert_into_item'      => _x('Insert into table', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'pretty-tables'),
            'uploaded_to_this_item' => _x('Uploaded to this table', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'pretty-tables'),
            'filter_items_list'     => _x('Filter tables list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'pretty-tables'),
            'items_list_navigation' => _x('tables list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'pretty-tables'),
            'items_list'            => _x('tables list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'pretty-tables'),
        );

        $args = array(
            'labels' => $labels,
            'public'             => true, //Have to finish. Will not be in menu
            'publicly_queryable' => true,
            'show_ui'            => true, //Have to finish. Will not be in menu
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'table'),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-games',
            'supports'           => array('title', 'editor', 'author', 'thumbnail'),
        );

        register_post_type('pretty-table', $args);
    }

    public function single_template_casino_slot($template)
    {
        if (is_singular('pretty-table')) {
            //Template for casino slot CPT
            require_once PRETTY_TABLES_BASE_DIR . 'public/class-pretty-tables-template-loader.php';

            $template_loader = new Pretty_Tables_Template_Loader();

            return $template_loader->get_template_part('single', 'pretty-table', false);
        }

        return $template;
    }
}
