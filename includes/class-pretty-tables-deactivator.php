<?php

/**
 * Fired during plugin deactivation
 *
 * @link       yoursite.lv
 * @since      1.0.0
 *
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pretty_Tables
 * @subpackage Pretty_Tables/includes
 * @author     MF <yoursite.lv>
 */
class Pretty_Tables_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		// Unregister CPT
		unregister_post_type('casino-review');

		unregister_post_type('casino-slot');

		//Flush Rewrite Rules
		flush_rewrite_rules();
	}

}
