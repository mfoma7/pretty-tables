<?php

/**
 * The plugin bootstrap file
 *
 * @link              yoursite.lv
 * @since             1.0.0
 * @package           Pretty_Tables
 *
 * @wordpress-plugin
 * Plugin Name:       Pretty Tables
 * Plugin URI:        yoursite.lv
 * Description:       Pretty Tables Plugin
 * Version:           1.0.0
 * Author:            MF
 * Author URI:        yoursite.lv
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pretty-tables
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PRETTY_TABLES_VERSION', '1.0.0' );

define('PRETTY_TABLES_NAME', 'pretty-tables');

//Plugin directory path
define('PRETTY_TABLES_BASE_DIR', plugin_dir_path(__FILE__));

//Plugin directory URL
define('PRETTY_TABLES_PLUGIN_URL', plugin_dir_url(__FILE__));

//Plugin directory URL
define('PRETTY_TABLES_IMG', plugin_dir_url(__FILE__) . "public/img/");

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-all-in-casino-activator.php
 */
function activate_pretty_tables() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pretty-tables-activator.php';
	Pretty_Tables_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pretty-tables-deactivator.php
 */
function deactivate_pretty_tables() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pretty-tables-deactivator.php';
	Pretty_Tables_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pretty_tables' );
register_deactivation_hook( __FILE__, 'deactivate_pretty_tables' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pretty-tables.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pretty_tables() {

	$plugin = new Pretty_Tables();
	$plugin->run();

}
run_pretty_tables();